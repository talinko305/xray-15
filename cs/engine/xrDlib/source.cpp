
// Copyright (C) 2006  Davis E. King (davis@dlib.net)
// License: Boost Software License   See LICENSE.txt for the full license.
#ifndef DLIB_ALL_SOURCe_
#define DLIB_ALL_SOURCe_

#if defined(DLIB_ALGs_) || defined(DLIB_PLATFORm_)
#include "X:/Projects/dlib_basic_cpp_build_tutorial.txt"
#endif

// ISO C++ code
#include "X:/Projects/base64/base64_kernel_1.cpp"
#include "X:/Projects/bigint/bigint_kernel_1.cpp"
#include "X:/Projects/bigint/bigint_kernel_2.cpp"
#include "X:/Projects/bit_stream/bit_stream_kernel_1.cpp"
#include "X:/Projects/entropy_decoder/entropy_decoder_kernel_1.cpp"
#include "X:/Projects/entropy_decoder/entropy_decoder_kernel_2.cpp"
#include "X:/Projects/entropy_encoder/entropy_encoder_kernel_1.cpp"
#include "X:/Projects/entropy_encoder/entropy_encoder_kernel_2.cpp"
#include "X:/Projects/md5/md5_kernel_1.cpp"
#include "X:/Projects/tokenizer/tokenizer_kernel_1.cpp"
#include "X:/Projects/unicode/unicode.cpp"
#include "X:/Projects/test_for_odr_violations.cpp"




#ifndef DLIB_ISO_CPP_ONLY
// Code that depends on OS specific APIs

// include this first so that it can disable the older version
// of the winsock API when compiled in windows.
#include "X:/Projects/sockets/sockets_kernel_1.cpp"
#include "X:/Projects/bsp/bsp.cpp"

#include "X:/Projects/dir_nav/dir_nav_kernel_1.cpp"
#include "X:/Projects/dir_nav/dir_nav_kernel_2.cpp"
#include "X:/Projects/dir_nav/dir_nav_extensions.cpp"
#include "X:/Projects/linker/linker_kernel_1.cpp"
#include "X:/Projects/logger/extra_logger_headers.cpp"
#include "X:/Projects/logger/logger_kernel_1.cpp"
#include "X:/Projects/logger/logger_config_file.cpp"
#include "X:/Projects/misc_api/misc_api_kernel_1.cpp"
#include "X:/Projects/misc_api/misc_api_kernel_2.cpp"
#include "X:/Projects/sockets/sockets_extensions.cpp"
#include "X:/Projects/sockets/sockets_kernel_2.cpp"
#include "X:/Projects/sockstreambuf/sockstreambuf.cpp"
#include "X:/Projects/sockstreambuf/sockstreambuf_unbuffered.cpp"
#include "X:/Projects/server/server_kernel.cpp"
#include "X:/Projects/server/server_iostream.cpp"
#include "X:/Projects/server/server_http.cpp"
#include "X:/Projects/threads/multithreaded_object_extension.cpp"
#include "X:/Projects/threads/threaded_object_extension.cpp"
#include "X:/Projects/threads/threads_kernel_1.cpp"
#include "X:/Projects/threads/threads_kernel_2.cpp"
#include "X:/Projects/threads/threads_kernel_shared.cpp"
#include "X:/Projects/threads/thread_pool_extension.cpp"
#include "X:/Projects/threads/async.cpp"
#include "X:/Projects/timer/timer.cpp"
#include "X:/Projects/stack_trace.cpp"

#ifdef DLIB_PNG_SUPPORT
#include "X:/Projects/image_loader/png_loader.cpp"
#include "X:/Projects/image_saver/save_png.cpp"
#endif

#ifdef DLIB_JPEG_SUPPORT
#include "X:/Projects/image_loader/jpeg_loader.cpp"
#include "X:/Projects/image_saver/save_jpeg.cpp"
#endif

#ifndef DLIB_NO_GUI_SUPPORT
#include "X:/Projects/gui_widgets/fonts.cpp"
#include "X:/Projects/gui_widgets/widgets.cpp"
#include "X:/Projects/gui_widgets/drawable.cpp"
#include "X:/Projects/gui_widgets/canvas_drawing.cpp"
#include "X:/Projects/gui_widgets/style.cpp"
#include "X:/Projects/gui_widgets/base_widgets.cpp"
#include "X:/Projects/gui_core/gui_core_kernel_1.cpp"
#include "X:/Projects/gui_core/gui_core_kernel_2.cpp"
#endif // DLIB_NO_GUI_SUPPORT

#include "X:/Projects/cuda/cpu_dlib.cpp"
#include "X:/Projects/cuda/tensor_tools.cpp"
#include "X:/Projects/data_io/image_dataset_metadata.cpp"
#include "X:/Projects/data_io/mnist.cpp"
#include "X:/Projects/data_io/cifar.cpp"
#include "X:/Projects/svm/auto.cpp"
#include "X:/Projects/global_optimization/global_function_search.cpp"
#include "X:/Projects/filtering/kalman_filter.cpp"

#endif // DLIB_ISO_CPP_ONLY





#define DLIB_ALL_SOURCE_END

#endif // DLIB_ALL_SOURCe_
